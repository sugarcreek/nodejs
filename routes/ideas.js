const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const {ensureAuthenticated} = require('../helpers/auth.js');

//////////////////////////////////////
// models
//////////////////////////////////////
require('../models/Idea');
const Idea = mongoose.model('ideas');


//////////////////////////////////////
// routes
//////////////////////////////////////
// Idea index
router.get('/', ensureAuthenticated, (req, res) => {
    Idea.find({user: req.user.id}).sort( {date: 'desc'} )
                 .then( ideas => { res.render('ideas/index', { ideas: ideas }) });
});

// Show 'Edit idea' form
router
    .get('/edit/:id', ensureAuthenticated, (req, res) => {
        Idea.findOne( {_id: req.params.id } )
            .then(idea => { if (idea.user != req.user.id) {
                                req.flash('error_msg', 'Not authorized!');
                                res.redirect('/ideas');
                            } else {               
                                res.render('ideas/edit', {idea: idea}); 
                            }
            })
            .catch( err => { console.log(err); return; });
        })
    .post('/edit/:id', ensureAuthenticated, (req, res) => {
        Idea.findOne( {_id: req.params.id} )
            .then( idea => { idea.title = req.body.title;
                             idea.details = req.body.details;
                             idea.save()
                                 .then( idea => { req.flash('success_msg','Project idea update.');
                                                  res.redirect('/ideas'); });
            })
    });

// Show 'Add idea' form
router
    .get('/add', ensureAuthenticated, (req, res) => { 
        res.render('ideas/add');
    })
    .post('/add', ensureAuthenticated, (req, res) => {
        //console.log(req.body); //enabled by body-parser
        let errors = [];
        if (!req.body.title) {
            errors.push({msg: 'Please enter a title.'});
        }
        if (!req.body.details) {
            errors.push({msg: 'Please enter some details.'});
        }
        if (errors.length) {
            res.render('ideas/add', {errors: errors, title: req.body.title, details: req.body.details});
        } else {
            //res.send('passed validation.');
            const newProject = { title: req.body.title, details: req.body.details, user: req.user.id };
            new Idea(newProject).save()
                                .then( idea => { req.flash('success_msg','Project idea added.');
                                                 res.redirect('/ideas'); } );
        }
    });
    

// Process 'Delete idea' form
router.post('/delete/:id', ensureAuthenticated, (req, res) => {
    Idea.remove( {_id: req.params.id} )
        .then( () => { req.flash('success_msg','Project idea deleted.');
                       res.redirect('/ideas'); });
});


module.exports = router;

