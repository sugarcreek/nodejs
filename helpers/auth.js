module.exports = {
    ensureAuthenticated: function(req, res, next){
        if (req.isAuthenticated()) { // comes from passport
            return next();
        }
        req.flash('error_msg', 'Not authorized!');
        res.redirect('/users/login');
    }
}