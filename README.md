# README #

Silly app for learning NodeJS with MongoDB and a handful of Javascript libraries.
The app collects project ideas (basic CRUD) and stores them in a NoSQL document database.

The app will be deployed to a AWS or DO cloud using a variety of tools (Jenkins, Ansible, Chef, Travis, ..) and find out which work best for me.
