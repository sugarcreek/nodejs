const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//////////////////////////////////////
// models
//////////////////////////////////////
require('../models/User');
const User = mongoose.model('users');

module.exports = function(passport){
    passport.use( new LocalStrategy( {usernameField: 'email'}, (email, password, done) => {
        //console.log(email);
        User.findOne( {email: email} )
            .then( user => { if (!user){  // if user does not exist, shout it out
                               // done(error, user, msg)
                               return done(null, false, {message: 'User with given email does not exist.'});
                             }
                             // We found the given user/email, check password
                             bcrypt.compare(password, user.password, (err, isMatch) => {
                                 if (err) throw err;
                                 if (isMatch) {
                                     // done(error, user, msg)
                                     return done(null, user);
                                 } else {
                                     return done(null, false, {message: 'Incorrect password.'});
                                 }
                             })
            })
    }) );
    
    // documentation specifies these 2 functions are required in order to obtain user info from cookie
    passport.serializeUser( function(user, done){ done(null, user.id); });
    passport.deserializeUser( function(id, done){
        // findById is a mongoose function. Re-write if not using mongoose.
        User.findById(id, function(err, user){ done(err, user); });
    });
}