// following ES6 as much as possible
const express = require('express');
const path = require('path');
const exphbs  = require('express-handlebars');
// not required
//const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('connect-flash');
const passport = require('passport');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
require('dotenv').config();

const app = express();
//const port = 5000;

// Passport config
require('./config/passport')(passport);

// The below spec doesn't work with mongoose
//mongoose.connect("mongodb://", { server: process.env.DB_HOST,
//                                 db:  process.env.DB_DATABASE,
//                                 user: process.env.DB_USER, 
//                                 pass: process.env.DB_PASS })

mongoose.Promise = global.Promise;
//mongodb://[username:password@]host1[:port1][,host2[:port2],...[,hostN[:portN]]][/[database][?options]]
const dbUrl = "mongodb://" + process.env.DB_USER + ':' +
                             process.env.DB_PASS + '@' +
                             process.env.DB_HOST + ':' +
                             process.env.DB_PORT + '/' +
                             process.env.DB_DATABASE;

// mongoose returns a promise
mongoose.connect(dbUrl)
    // catch promise with .then
    .then( () => console.log('Connected to MongoDB..') )
    .catch( err => console.log(err) );

mongoose.set('debug', true);
//mongoose.connection.on('connected', function () { console.log('connected'); });
mongoose.connection.on('error', function (err) { console.log(err); });
//mongoose.connection.on('disconnected', function () { console.log('disconnected'); });


//////////////////////////////////////
// middlewares
//////////////////////////////////////

// Assign the public static folder
app.use(express.static(path.join(__dirname, 'public')));

// Asign templating system
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// parse application/x-www-form-urlencoded
app.use(bodyparser.urlencoded({ extended: false }))
app.use(bodyparser.json())

// not required
//app.use(methodOverride('_method'));

var sess = {
    secret: process.env.SECRET,
    resave: true,
    saveUninitialized: true,
};
app.use(session(sess));

//passport session info needs to go AFTER Express session middleware
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

app.use(function(req, res, next){
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});

// Make sure ALL configuration is done BEFORE using routes
// use routes
app.use('/ideas', require('./routes/ideas'));
app.use('/users', require('./routes/users'));

// Home
app.get('/', (req, res) => {
    res.render('home', { title: 'Welcome' });
});
// About
app.get('/about', (req, res) => {
    res.render('about');
});


app.listen(process.env.APP_PORT, () => {
    console.log(`Server started on port ${process.env.APP_PORT}.`);
});